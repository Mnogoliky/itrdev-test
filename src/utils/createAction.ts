import {Action} from "../store/types";


export type ActionConstructor = (...props: any) => Action;

export const createAction = (type: string, funBody: (...props: any) => object): ActionConstructor => {
	const newFun = (...props: any): Action => ({type, ...funBody(...props)});
	newFun.toString = (): string => type;
	return newFun;
};