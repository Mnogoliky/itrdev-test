
export const reformatDate = (datestr: string) => {
    const date = new Date(datestr);
    const monthes: string[] = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ];
    return [
        date.getDay(),
        monthes[date.getMonth()],
        date.getFullYear()
    ].join(' ');
};