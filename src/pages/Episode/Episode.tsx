import React, {FunctionComponent, ReactNode, useEffect} from 'react'
import {connect} from 'react-redux'
import Helmet from 'react-helmet';
import {Button, Icon, Message} from 'semantic-ui-react'
import {StateInterface} from "../../store/types";
import * as actions from '../../store/movies/actions'
import {Action, FilmInterface} from "../../store/types";
import {Preloader} from "../../components";
import {reformatDate} from "../../utils/functions";

interface ListContainerProps extends Connect {
    dispatch: (action: Action) => void,
    match: {
        params: {
            id: string
        },

    },
    history: {
        goBack: () => void
    }
}

const EpisodeContainer = (props: ListContainerProps): ReactNode => {

    console.log(props);

    const {
        dispatch,
        loaded,
        current: {
            title,
            release_date,
            opening_crawl,
            director,
            producer
        },
        match: {
            params: {id}
        },
        history: {
            goBack
        }
    } = props;


    useEffect(() => {
        dispatch(actions.getEpisode(parseInt(id)))
    }, [false]);


    if (!loaded)
        return (
            <>
                <Helmet title = ''/>
                <Preloader/>
            </>
        );
    else
        return (
            <div>
                <Helmet title = {title}/>
                <Button onClick={goBack} icon labelPosition = 'left'>
                    <Icon name = 'arrow left'/>
                    Back
                </Button>
                <Message>
                    <Message.Header>Title</Message.Header>
                    <p>
                        {title}
                    </p>
                </Message>
                <Message>
                    <Message.Header>Opening crawl</Message.Header>
                    <p>
                        {opening_crawl}
                    </p>
                </Message>
                <Message>
                    <Message.Header>Director</Message.Header>
                    <p>
                        {director}
                    </p>
                </Message>
                <Message>
                    <Message.Header>Producer</Message.Header>
                    <p>
                        {producer}
                    </p>
                </Message>
                <Message>
                    <Message.Header>Release date</Message.Header>
                    <p>
                        {reformatDate(release_date)}
                    </p>
                </Message>
            </div>
        )
};

interface Connect {
    current: FilmInterface,
    loaded: boolean,
}

export const Episode = connect((state: StateInterface): Connect => ({
    current: state.movies.current,
    loaded: state.movies.loaded,
}))(EpisodeContainer as FunctionComponent);