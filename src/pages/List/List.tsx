import React, {FunctionComponent, ReactNode, useEffect, useState} from 'react'
import {connect} from 'react-redux'
import Helmet from 'react-helmet';
import {Link} from 'react-router-dom'
import {Input, Table, Message} from 'semantic-ui-react'
import {StateInterface} from "../../store/types";
import * as actions from '../../store/movies/actions'
import {Action, FilmInterface} from "../../store/types";
import {Preloader} from "../../components";

interface ListContainerProps extends Connect {
    dispatch: (action: Action) => void,
}

const ListContainer = (props: ListContainerProps): ReactNode => {

    const {dispatch, list, loaded} = props;

    useEffect(() => {
        dispatch(actions.getMovies())
    }, [false]);

    const [searchText, setSearchText] = useState('');

    const filterList: FilmInterface[] = list.filter(item => item.title.toLowerCase().includes(searchText.toLowerCase()));

    if (!loaded)
        return <Preloader/>;
    else
        return (
            <div style = {{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <Helmet title = 'Star wars episodes'/>
                <Input
                    icon = 'search'
                    placeholder = 'Search...'
                    value = {searchText}
                    onChange = {(e, data) => setSearchText(data.value)}
                />
                {
                    filterList.length ?
                        (
                            <Table celled selectable>
                                <Table.Body>
                                    {
                                        filterList.map((item: FilmInterface) => (
                                            <Table.Row key = {item.episode_id}>
                                                <Link to = {`/films/${item.episode_id}`}>
                                                    <Table.Cell>{item.title}</Table.Cell>
                                                </Link>
                                            </Table.Row>
                                        ))
                                    }
                                </Table.Body>
                            </Table>
                        ) :
                        (
                            <Message
                                icon = 'search'
                                header = 'No Results'
                            />
                        )
                }
            </div>
        )
};

interface Connect {
    list: FilmInterface[],
    loaded: boolean,
}

export const List = connect((state: StateInterface): Connect => ({
    list: state.movies.list,
    loaded: state.movies.loaded,
}))(ListContainer as FunctionComponent);