import React, {FunctionComponent} from 'react';
import {Loader} from 'semantic-ui-react';
import './styles.css'

export const Preloader: FunctionComponent = () => {

    return (
        <div className = 'preloader'>
            <Loader active/>
        </div>
    )
};