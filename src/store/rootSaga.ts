import { all } from 'redux-saga/effects';
import { watchAllMovies } from './movies/saga';

export function* rootSaga() {
    yield all([
        watchAllMovies(),
    ]);
}