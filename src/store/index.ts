import {
    createStore,
    combineReducers,
    applyMiddleware,
    compose,
} from 'redux';

import movies from './movies/reducer';

import createSagaMiddleware from 'redux-saga';
import {rootSaga} from './rootSaga';

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootReducers = combineReducers({
    movies,
});
const sagaMiddleware = createSagaMiddleware();
export const store = createStore(rootReducers, composeEnhancers(applyMiddleware(sagaMiddleware,)));

sagaMiddleware.run(rootSaga);