import {MoviesState} from "./movies/reducer";

export interface FilmInterface {
    title: string,
    episode_id: number,
    opening_crawl: string,
    director: string,
    producer: string,
    release_date: string,
    created: string,
    edited: string,
    url: string,
    charasters: string[],
    planets: string[],
    starships: string[],
    vehicles: string[],
    species: string[],
}

// interface Payload {
//     [key: string]: any,
// }

type Payload = any;

export interface Action {
    type: string,
    payload?: Payload,
}

export interface StateInterface {
    movies: MoviesState,
}