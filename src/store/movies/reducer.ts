import * as actions from './actions';
import {FilmInterface} from "../types";
import {Action} from "../types";

export interface MoviesState {
    list: FilmInterface[],
    loaded: boolean,
    isLoaded: boolean
    current: FilmInterface
};

const initialState: MoviesState = {
    list: [],
    loaded: false,
    isLoaded: false,
    current: {
        producer: '',
        director: '',
        opening_crawl: '',
        title: '',
        created: '',
        url: '',
        release_date: '',
        episode_id: 0,
        edited: '',
        charasters: [],
        planets: [],
        species: [],
        starships: [],
        vehicles: [],
    },
};


const moviesReducer = (state: MoviesState = initialState, action: Action): MoviesState => {
    interface HandlersInterface {
        [key: string]: (state: MoviesState, action: Action) => MoviesState,
    }

    const handlers: HandlersInterface = {
        [actions.setMovies.toString()]: (state: MoviesState, {payload}: Action) => ({
            ...state,
            list: payload.list,
        }),
        [actions.setLoadStatus.toString()]: (state: MoviesState, {payload}: Action) => ({
            ...state,
            loaded: payload.status,
        }),
        [actions.setCurrent.toString()]: (state: MoviesState, {payload}: Action) => ({
            ...state,
            current: payload.data,
        }),
        [actions.setIsLoadedStatus.toString()]: (state: MoviesState, {payload}: Action) => ({
            ...state,
            isLoaded: payload.status,
        }),
    };
    return action.type in handlers ? handlers[action.type](state, action) : state;
};

export default moviesReducer;