import {all, takeEvery, put, call, select} from 'redux-saga/effects';
import * as api from './api';
import * as actions from './actions';
import {Action, FilmInterface, StateInterface} from "../types";

function* getAllMovies() {
    interface ResponseInterface {
        count: number,
        next: any,
        previos: any,
        results: FilmInterface[]
    }

    try {
        const isLoaded: boolean = yield select((state: StateInterface) => state.movies.isLoaded);
        if (!isLoaded) {
            yield put(actions.setLoadStatus(false));
            const data: ResponseInterface = yield call(api.getAllFilms);
            yield put(actions.setMovies(data.results));
            yield put(actions.setIsLoadedStatus(true));
            yield put(actions.setLoadStatus(true));
        }
    }
    catch (e) {
        console.error(e)
    }
}

function* getEpisode(action: Action) {
    const id: number = action.payload.id;
    try {
        yield getAllMovies();
        const list: FilmInterface[] = yield select((state: StateInterface) => state.movies.list);
        yield put(actions.setCurrent(list.find((item: FilmInterface) => item.episode_id === id) || {}))
    }
    catch (e) {

    }

}

export function* watchAllMovies() {
    yield all([
        takeEvery(actions.getMovies.toString(), getAllMovies),
        takeEvery(actions.getEpisode.toString(), getEpisode),
    ]);
}
