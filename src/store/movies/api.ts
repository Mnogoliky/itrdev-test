
export const getAllFilms = () =>
    fetch('https://swapi.co/api/films/')
        .then(r => {
            if (r.ok)
                return r.json();
            else
                throw {message: r.statusText}
        });