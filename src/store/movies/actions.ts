import {createAction, ActionConstructor} from "../../utils/createAction";
import {FilmInterface} from "../types";

const family: string = '@movies/';

export const getMovies: ActionConstructor = createAction(family + 'GET_MOVIES', () => ({}));
export const setMovies: ActionConstructor = createAction(family + 'SET_MOVIES', (list: FilmInterface[]) => ({payload: {list}}));

export const setLoadStatus: ActionConstructor = createAction(family + 'SET_LOAD_STATUS', (status: boolean) => ({payload: {status}}));

export const getEpisode: ActionConstructor = createAction(family + 'GET_EPISODE', (id: number) => ({payload: {id}}));
export const setCurrent: ActionConstructor = createAction(family + 'SET_CURRENT', (data: FilmInterface) => ({payload: {data}}));

export const setIsLoadedStatus: ActionConstructor = createAction(family + 'SET_IS_LOADED_STATUS', (status: boolean) => ({payload: {status}}));
