import React from 'react';
import {Switch, Route, Redirect} from 'react-router'
import {List, Episode} from "./pages";

const App: React.FC = () => {
    return (
        <div className = "App">
            <Switch>
                <Route path = '/films/:id' component = {Episode}/>
                <Route path = '/films' component = {List}/>
                <Redirect to = '/films'/>
            </Switch>
        </div>
    );
};

export default App;
