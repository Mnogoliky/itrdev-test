## Тестовое задание для ITRDev

#### Библиотеки:
- [`typescript`](https://www.npmjs.com/package/typescript)
- [`redux`](https://www.npmjs.com/package/redux)
- [`react-redux`](https://www.npmjs.com/package/react-redux)
- [`redux-saga`](https://www.npmjs.com/package/redux-saga)
- [`react-router`](https://www.npmjs.com/package/react-router)
- [`react-helmet`](https://www.npmjs.com/package/react-helmet)
- [`semantic-ui-react`](https://www.npmjs.com/package/semantic-ui-react) [`semantic-ui-css`](https://www.npmjs.com/package/semantic-ui-css)